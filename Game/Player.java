
public class Player implements PlayerInterface
{
	private String name;
	private String ID;
	private Hand myHand;
	private int score;
	private boolean isBot;
	Player()
	{
		name ="";
		ID = "";
		myHand = new Hand(7);
		score = 0;
		isBot = false;
	}
	Player(String name, String ID, int max)
	{
		this.name = name;
		this.ID = ID;
		myHand = new Hand(max);
		score = 0;
		isBot = false;
	}
	public String getName()				// beginning of get and set methods
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getID()
	{
		return ID;
	}
	public void setID(String ID)
	{
		this.ID = ID;
	}
	public Hand getHand()
	{
		return myHand;
	}
	public void setHand(Hand myHand)
	{
		this.myHand = myHand;
	}
	public int getScore()
	{
		return score;
	}
	public void setScore(int score)		
	{
		this.score = score;
	}
	public void setBot()
	{
		isBot = !isBot;
	}
	public boolean getBot()				// end of get and set methods.
	{
		return isBot;
	}
	// postcondition: this method will add the temp card into player's hand
	// return: void
	public void addOneCard(Card temp)
	{
		if(this.getHand().handIsFull())
			System.out.println("my hand is full!");
		else
			this.getHand().insertOneCardToHand(temp);
	}
	// precondition: instance of player that is calling this method must not be null.
	// postcondition: this method will gather all information about a player.
	// return: a string that meets postcondition. 
	public String toString()
	{
		return "Player name:" + this.getName()+"\n Player ID: "+this.getID()+"\n"+this.getHand().toString();
	}
	public static void main(String[] args) 
	{
		Deck myDeck = new Deck();
		myDeck.add(new Card("3","spades"));
		myDeck.add(new Card("4","spades"));
		myDeck.add(new Card("5","spades"));
		myDeck.add(new Card("6","spades"));
		myDeck.add(new Card("7","spades"));
		myDeck.add(new Card("8","spades"));
		myDeck.add(new Card("9","spades"));
		myDeck.add(new Card("10","spades"));
		myDeck.add(new Card("11","spades"));
		myDeck.shuffling();
		Player me = new Player("jiajun","0001",7);
		for(int i=myDeck.getCount()-me.getHand().getMax()+1;i<=myDeck.getCount();)
		{
			if(!me.getHand().handIsFull())
			{
				
				me.getHand().insertOneCardToHand(new Card(myDeck.getTopCard().getFace(),myDeck.getTopCard().getSuit()));
				myDeck.remove(myDeck.getTopCard());
				System.out.println(myDeck.getCount());
			}
		}
		System.out.println("after taking cards, cards in the deck pile: ");
		System.out.println(myDeck.toString());
		
		System.out.println("\n what's in the hand pile: ");
		System.out.println(me.getHand().toString());
	}

}
