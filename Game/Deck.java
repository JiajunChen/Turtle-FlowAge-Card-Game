public class Deck extends Pile implements DeckInterface
{
	Deck()
	{
		super();
	}
	// precondition: nothing
	// postcondition: this method will put all information in the deck pile into a string
	// return: the resulting string will all information about the deck pile.
	public String toString()
	{
		return "what's in the deck pile: \n"+super.toString();
	}
	// precondition: nothing
	// postcondition: this method will take one card from the deck pile
	// return: top card or null if it's empty.
	public Card takeOneCard()
	{
		return super.remove(this.getTopCard());
	}
	// precondition: nothing
	// postcondition: this method will determine whether the deck pile is empty or not
	// return: a boolean.
	public boolean isEmpty()
	{
		return this.getCount()<=0;
	}
	public static void main(String[] args) 
	{
		Deck myDeck = new Deck();
		myDeck.add(new Card("3","spades"));
		myDeck.add(new Card("4","spades"));
		myDeck.add(new Card("5","spades"));
		myDeck.add(new Card("6","spades"));
		myDeck.add(new Card("7","spades"));
		myDeck.add(new Card("8","spades"));
		myDeck.add(new Card("9","spades"));
		myDeck.add(new Card("10","spades"));
		myDeck.add(new Card("11","spades"));
		System.out.println("what's in the pile: ");
		System.out.println(myDeck.toString());
		System.out.println("\n I took one card from the deck pile:");
		System.out.println(myDeck.takeOneCard().toString());
		System.out.println("then, what's left in the deck pile: ");
		System.out.println(myDeck.toString());
	}

}
