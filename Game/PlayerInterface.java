
public interface PlayerInterface 
{
		// postcondition: this method will add the temp card into player's hand
		// return: void
		public void addOneCard(Card temp);
		
		// precondition: instance of player that is calling this method must not be null.
		// postcondition: this method will gather all information about a player.
		// return: a string that meets postcondition. 
		public String toString();
		
}
