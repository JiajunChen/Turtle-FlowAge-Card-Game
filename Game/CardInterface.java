
public interface CardInterface 
{
		// precondition: entry card CAN NOT BE NULL
		// postcondition: determine corresponding order of the entry card
		// return:	returns an int that matches the order of special card rule.
		public int determineSpecialCardValue(Card entry);
		
		// precondition: instance of card class that is calling this method CAN NOT BE NULL!
		// postcondition: this method will determine whether the card is a special card or not
		// return: true if the card is a special card, false otherwise.
		public boolean isSpecialCard();
		
		// precondition: entry card CAN NOT BE NULL
		// postcondition: this method will determine the value for the entry card
		// return: corresponding value for the entry card. 
		public int determineValue(Card entry);
		
		// precondition: entry card can not be null
		// postcondition: determine the suit of the entry card
		// return: an integer that is corresponding to the entry card's suit.
		public int determineSuit(Card entry);
		
		// precondition: Card CAN NOT BE NULL
		// return: returns information about the card as a string
		public String toString();
		
}
