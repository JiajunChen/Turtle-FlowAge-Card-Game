import java.util.ArrayList;
import java.util.Scanner;
public class Pile implements PileInterface
{
	private Card topOfPile;
	private Card bottomOfPile;
	private int count;
	Pile()			// default constructor
	{
		topOfPile = null;
		bottomOfPile = null;
		count = 0;
	}
	public Card getTopCard()			// begining of the getor and setor
	{
		return topOfPile;
	}
	public void setTopCard(Card entry)
	{
		topOfPile = entry;
	}
	public Card getBottomCard()
	{
		return bottomOfPile;
	}
	public void setBottomCard(Card entry)
	{
		bottomOfPile = entry;
	}
	public int getCount()
	{
		return count;
	}
	public void setCount(int count)			// ending of the getor and setor
	{
		this.count = count;
	}
	// precondition: nothing
	// postcondition: collect informations about the pile in a resulting string
	// return: resulting string 
	public String toString()
	{
		String result = "";
		Card temp = this.getTopCard();
		while(temp!=null)
		{
			result += temp.toString() + "\n";
			temp = temp.getBelow();
		}
		return result;
	}
	// precondition: nothing
	// postcondition: insert the new card to the end of the pile 
	// return: void
	public void add(Card entry)
	{
		entry.setAbove(null);
		entry.setBelow(null);
		this.setCount(this.getCount()+1);
		if(this.getBottomCard()!=null)
		{
			this.getBottomCard().setBelow(entry);
			entry.setAbove(this.getBottomCard());
			this.setBottomCard(entry);
		}
		else
		{
			this.setTopCard(entry);
			this.setBottomCard(entry);
		}
	}
	// precondition: entry card can not be null
	// postcondition: search through the pile and find the entry card.
	// return: return card if found it, null other wise.
	public Card search(Card entry)
	{
		Card temp = this.getTopCard();
		while(temp!=null)
		{
			if(temp.compareTo(entry)==0&& temp.determineSuit(temp)==entry.determineSuit(entry))
				return temp;
			temp = temp.getBelow();
		}
		return null;
	}
	// precondition: entry card can not be null
	// postcondition: this method will remove the entry card from the pile
	// return: Card that is being removed
	public Card remove(Card entry)
	{
		Card temp = this.search(entry);
		if(temp!=null)
		{
			this.setCount(this.getCount()-1);
			if(temp == this.getTopCard())		// CASE 1: removing the top card of the pile, including there is only one
			{									// card in the pile, which is both being considered as top and bottom of the pile
				if(this.getCount()==1)
					this.setTopCard(null);		// CASE 1.1: there is only one card in the pile
				else
				{
					this.setTopCard(this.getTopCard().getBelow());	// CASE 1.2: there is more than one card in the pile
					this.getTopCard().setAbove(null);
				}
			}
			else
			{
				if(temp == this.getBottomCard()) // CASE 2: removing the bottom card of the pile
				{
					this.setBottomCard(this.getBottomCard().getAbove());
					this.getBottomCard().setBelow(null);
				}
				else							 // CASE 3: removing the card at the middle of the pile
				{
					temp.getAbove().setBelow(temp.getBelow());
					temp.getBelow().setAbove(temp.getAbove());
				}
			}
		}
		return temp;
	}
	// precondition: nothing
	// postcondition: shuffle the pile
	// return: void
	public void shuffling()
	{
		if(this.getCount()>=2)
		{
			// filling the pile into an array
			Card temp = this.getTopCard();
			Card[] cardArray = new Card[this.getCount()];
			for(int i=0;i<this.getCount()&&temp!=null;i++,temp=temp.getBelow())
			{
				int foo = (int)(Math.random()*this.getCount());
				while(cardArray[foo]!=null)
					foo = (int)(Math.random()*this.getCount());
				cardArray[foo] = temp;					
			}
			// collect cards back to pile 
			this.setTopCard(cardArray[0]);
			this.getTopCard().setAbove(null);			// top of the pile
			this.getTopCard().setBelow(null);
			temp = this.getTopCard();
			for(int i=1;i<cardArray.length-1;i++,temp = temp.getBelow())
			{
				temp.setBelow(cardArray[i]);			// middle of the pile
				cardArray[i].setAbove(temp);
			}
			temp.setBelow(cardArray[cardArray.length-1]);
			cardArray[cardArray.length-1].setAbove(temp);	// bottom of the pile
			cardArray[cardArray.length-1].setBelow(null);
			this.setBottomCard(cardArray[cardArray.length-1]);
		}
	}
	// precondition: nothing
	// postcondition: this method will sort the pile in increasing order
	// return: void
	public void sorting()
	{
		if(this.getCount()>=2)
		{
			for(Card temp = this.getTopCard();temp!=null;temp= temp.getBelow())
			{
				Card smallest = temp;
				for(Card foo = temp;foo!=null;foo= foo.getBelow())
				{
					if(smallest.compareTo(foo)>0)
						smallest = foo;
				}
				Card dummy = new Card(smallest.getFace(),smallest.getSuit());
				smallest.setFace(temp.getFace());
				smallest.setSuit(temp.getSuit());
				temp.setFace(dummy.getFace());
				temp.setSuit(dummy.getSuit());
			}
		}
	}
	// precondition: argument must be int
	// postcondition: this method will split the pile into num of piles
	// return: a resulting arratlist of piles
	public ArrayList<Pile> splitting(int num)
	{
		ArrayList<Pile> result = new ArrayList<Pile>();
		for(int i=1;i<=num;i++)
		{
			result.add(new Pile());
		}
		Card temp = this.getTopCard();
		System.out.println("the second card in the pile is: "+this.getTopCard().getBelow());
		for(int i=0;i<this.getCount();i++,temp = temp.getBelow())
		{
			result.get(i%num).add(new Card(temp.getFace(),temp.getSuit()));
		}
		return result;
	}
	// precondition: nothing
	// postcondition: this method will discard all cards in the pile
	// return: void
	public void clearPile()
	{
		this.setTopCard(null);
		this.setBottomCard(null);
		this.setCount(0);
	}
	// postcondition: this method will determine is the entry pile is 1x, 2x, 3x or 4x card pile
	// return: 1,2,3,or 4. depends on the pile.
	public int determinePile(Pile entry)
	{
		int count = entry.getCount();
		entry.sorting();
		for(Card temp = entry.getTopCard();temp.getBelow()!=null;temp= temp.getBelow())
		{
			if(temp.determineValue(temp)==temp.getBelow().determineValue(temp.getBelow()))
				count++;
			else
				return -1;
		}
		return count;
	}
	public static void main(String[] args) 
	{
		Pile myPile = new Pile();
		myPile.add(new Card("3","spades"));
		myPile.add(new Card("4","spades"));
		myPile.add(new Card("5","spades"));
		myPile.add(new Card("6","spades"));
		myPile.add(new Card("7","spades"));
		myPile.add(new Card("8","spades"));
		myPile.add(new Card("9","spades"));
		myPile.add(new Card("10","spades"));
		myPile.add(new Card("11","spades"));
		
		System.out.println("what's in the pile: ");
		System.out.println(myPile.toString());
		System.out.println("after remove(3,spades):");
		myPile.remove(new Card("3","spades"));
		System.out.println(myPile.toString());
		System.out.println("remove(3,spades) again!");
		myPile.remove(new Card("3", "spades"));
		System.out.println(myPile.toString());
		System.out.println("is spades 3 in the pile? ");
		if(myPile.search(new Card("3","spades")) == null)
			System.out.println("no");
		else
			System.out.println("yes");
		System.out.println("after the pile is shuffled: ");
		myPile.shuffling();
		System.out.println(myPile.toString());
		System.out.println("there is "+myPile.getCount()+ " cards in the pile!");

		System.out.println("\n\n");
		System.out.println("reminder, this is 7-1-5-2-3-K-Q-J-10-9-8-6-4 <from greatest card to smallest card:> ");
		myPile.sorting();
		System.out.println(myPile.toString());
		
		System.out.println("splitting testing, please enter the number of piles you want to split");
		Scanner key = new Scanner (System.in);
		ArrayList<Pile> temp =  myPile.splitting(key.nextInt());
		for(int i=0;i<temp.size();i++)
		{
			System.out.println("cards in pile "+i);
			System.out.println(temp.get(i).toString());
		}
	}

}
