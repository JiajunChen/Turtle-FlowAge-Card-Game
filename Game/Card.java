
public class Card implements Comparable<Card>, CardInterface
{
	private Card Above;
	private Card Below;
	private String face;
	private String suit;
	Card()			// default constructor.
	{
		Above = null;
		Below = null;
		face = " ";
		suit = " ";
	}
	Card(String face, String suit)			// constructor.
	{
		Above = null;
		Below  = null;
		this.face =face;
		this.suit = suit;
	}
	public Card getAbove()			// getor and setor begins here
	{
		return Above;
	}
	public void setAbove(Card above)
	{
		Above = above;
	}
	public Card getBelow()
	{
		return Below;
	}
	public void setBelow(Card below)
	{
		Below = below;
	}
	public String getFace()
	{
		return face;
	}
	public void setFace(String f)
	{
		face =f;
	}
	public String getSuit()
	{
		return suit;
	}
	public void setSuit(String s)			// getor and setor ends here
	{
		suit =s;
	}
	
	// precondition: cards that is being comparing CAN NOT BE NULL
	// postcondition: this method will compare two cards using the rule of the GAME_71523
	// return: 		case 1:  return 0 if two cards are equal
	//				case 2:  return positive int if first card is bigger than the second
	//				case 3:  return negative int if first card is less than the second
	public int compareTo(Card entry)
	{
		int value1 = this.determineValue(this);
		int value2 = entry.determineValue(entry);
		if(this.isSpecialCard())
		{
			if(!entry.isSpecialCard())  // CASE 1: card one is special and card 2 is not 
				return 1;
			else						// CASE 2: card one and card 2 both special
			{
				return this.determineSpecialCardValue(this)-entry.determineSpecialCardValue(entry);
				//will return 0 if they are the same card value. 
			}
		}
		else
		{
			if(entry.isSpecialCard())	// CASE 3: card one is not special but card 2 is
				return -1;
			else						// case 4: card one and card 2 both not special card
			{
				return value1-value2;
				// will return 0 if they are the same card value.
			}
		}
		
	}
	// precondition: entry card CAN NOT BE NULL
	// postcondition: determine corresponding order of the entry card
	// return:	returns an int that matches the order of special card rule.
	public int determineSpecialCardValue(Card entry)
	{
		int value1 = entry.determineValue(entry);
		switch(value1)
		{
			case 7:
				return 5;
			case 1:
				return 4;
			case 5:
				return 3;
			case 2:
				return 2;
			case 3:
				return 1;
			default:
				return 0;
		}
	}
	// precondition: instance of card class that is calling this method CAN NOT BE NULL!
	// postcondition: this method will determine whether the card is a special card or not
	// return: true if the card is a special card, false otherwise.
	public boolean isSpecialCard()
	{
		int value = this.determineValue(this);
		switch(value)
		{
			case 7: 
			case 1:
			case 5:
			case 2:
			case 3:
				return true;
			default:
				return false;
		}
	}
	// precondition: entry card CAN NOT BE NULL
	// postcondition: this method will determine the value for the entry card
	// return: corresponding value for the entry card. 
	public int determineValue(Card entry)
	{
		String temp = entry.getFace();
		int value1 = 0;
		switch(temp.toLowerCase())
		{
		case "ace": case "a": case "1":
		{
			value1 = 1;
			break;
		}
		case "king": case "k": case "13":
		{
			value1 = 13;
			break;
		}
		case "queen": case "q": case "12":
		{
			value1 = 12;
			break;
		}
		case "jack": case "j": case "11":
		{
			value1 = 11;
			break;
		}
		case "ten": case "10":
		{
			value1 = 10;
			break;
		}
		case "nine": case "9":
		{
			value1 = 9;
			break;
		}
		case "eight": case "8":
		{
			value1 = 8;
			break;
		}
		case "seven": case "7":
		{
			value1 = 7;
			break;
		}
		case "six": case "6":
		{
			value1 = 6;
			break;
		}
		case "five": case "5":
		{
			value1 = 5;
			break;
		}
		case "four": case "4":
		{
			value1 = 4;
			break;
		}
		case "three": case "3":
		{
			value1 = 3;
			break;
		}
		case "two": case "2":
			value1 = 2;
		default: 
			break; 
		}
		return value1;
	}
	// precondition: entry card can not be null
	// postcondition: determine the suit of the entry card
	// return: an integer that is corresponding to the entry card's suit.
	public int determineSuit(Card entry)
	{
		switch(entry.getSuit().toLowerCase())
		{
			case "spades": case "spade":
				return 1;
			case "hearts": case "heart":
				return 2;
			case "diamonds": case "dismond":
				return 3;
			case "clubs": case "club":
				return 4;
			default:
				return 0;	
		}
	}
	// precondition: Card CAN NOT BE NULL
	// return: returns information about the card as a string
	public String toString()
	{
		if(this==null)
			return "";
		return this.getSuit() + " " +this.determineValue(this);
	}
	public static void main(String[] args) 
	{
		Card one = new Card("seven","spades");
		Card two = new Card("seven","hearts");
		Card three = new Card("king","clubs");
		System.out.println("information of three cards: ");
		System.out.println(one.toString());
		System.out.println(two.toString());
		System.out.println(three.toString());
		System.out.println("\n card one compareTo card two: "+one.compareTo(two));
		System.out.println("\n card one compareTo card three: "+one.compareTo(three));
		
		one.setAbove(null);
		one.setBelow(two);
		two.setAbove(one);
		two.setBelow(null);

		System.out.println("Card one: "+one.toString());
		System.out.println("Below card one: "+one.getBelow().toString());
		System.out.println("Card two: "+ two.toString());
		System.out.println("Above two: "+two.getAbove().toString());
	}
	
}
