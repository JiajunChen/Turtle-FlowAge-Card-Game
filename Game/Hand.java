
public class Hand extends Pile implements HandInterface
{
	private int maxNumCard;
	Hand(int max)
	{
		super();
		maxNumCard = max;
	}
	public int getMax()
	{
		return maxNumCard;
	}
	public void setMax(int num)
	{
		maxNumCard = num;
	}
	// precondition: nothing
	// postcondition: this method will collect all information about hand in a string
	// return: a string containing all the information about hand.
	public String toString()
	{
		return "what are the cards in hand: \n"+super.toString();
	}
	// precondition: nothing
	// postcondition: this method will add a card to hand if hand is not full;
	// return: void
	public void insertOneCardToHand(Card temp)
	{
		if(!handIsFull())
			super.add(temp);
	}
	// precondition: nothing
	// position: this method will determine if the hand is full
	// return: boolean
	public boolean handIsFull()
	{
		return super.getCount()>=maxNumCard;
	}
	public static void main(String[] args) 
	{
		Deck myDeck = new Deck();
		myDeck.add(new Card("3","spades"));
		myDeck.add(new Card("4","spades"));
		myDeck.add(new Card("5","spades"));
		myDeck.add(new Card("6","spades"));
		myDeck.add(new Card("7","spades"));
		myDeck.add(new Card("8","spades"));
		myDeck.add(new Card("9","spades"));
		myDeck.add(new Card("10","spades"));
		myDeck.add(new Card("11","spades"));
		myDeck.shuffling();
		Hand myHand = new Hand(7);
		for(int i=myDeck.getCount()-myHand.getMax()+1;i<=myDeck.getCount();)
		{
			if(!myHand.handIsFull())
			{
				
				myHand.insertOneCardToHand(new Card(myDeck.getTopCard().getFace(),myDeck.getTopCard().getSuit()));
				myDeck.remove(myDeck.getTopCard());
				System.out.println(myDeck.getCount());
			}
		}
		System.out.println("after taking cards, cards in the deck pile: ");
		System.out.println(myDeck.toString());
		
		System.out.println("\n what's in the hand pile: ");
		System.out.println(myHand.toString());
	}

}
