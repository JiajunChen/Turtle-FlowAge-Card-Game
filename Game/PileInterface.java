import java.util.ArrayList;

public interface PileInterface 
{
		// precondition: nothing
		// postcondition: collect informations about the pile in a resulting string
		// return: resulting string 
		public String toString();
		
		// precondition: nothing
		// postcondition: insert the new card to the end of the pile 
		// return: void
		public void add(Card entry);
		
		// precondition: entry card can not be null
		// postcondition: search through the pile and find the entry card.
		// return: return card if found it, null other wise.
		public Card search(Card entry);
		
		// precondition: entry card can not be null
		// postcondition: this method will remove the entry card from the pile
		// return: Card that is being removed
		public Card remove(Card entry);
		
		// precondition: nothing
		// postcondition: shuffle the pile
		// return: void
		public void shuffling();
		
		// precondition: nothing
		// postcondition: this method will sort the pile in increasing order
		// return: void
		public void sorting();
		
		// precondition: argument must be int
		// postcondition: this method will split the pile into num of piles
		// return: a resulting arratlist of piles
		public ArrayList<Pile> splitting(int num);
		
		// precondition: nothing
		// postcondition: this method will discard all cards in the pile
		// return: void
		public void clearPile();
		
		// postcondition: this method will determine is the entry pile is 1x, 2x, 3x or 4x card pile
		// return: 1,2,3,or 4. depends on the pile.
		public int determinePile(Pile entry);
		
}
