
public interface DeckInterface 
{
		// precondition: nothing
		// postcondition: this method will put all information in the deck pile into a string
		// return: the resulting string will all information about the deck pile.
		public String toString();
		
		// precondition: nothing
		// postcondition: this method will take one card from the deck pile
		// return: top card or null if it's empty.
		public Card takeOneCard();
		
		// precondition: nothing
		// postcondition: this method will determine whether the deck pile is empty or not
		// return: a boolean.
		public boolean isEmpty();
		
		
}
