
public interface HandInterface 
{
		// precondition: nothing
		// postcondition: this method will collect all information about hand in a string
		// return: a string containing all the information about hand.
		public String toString();
		
		// precondition: nothing
		// postcondition: this method will add a card to hand if hand is not full;
		// return: void
		public void insertOneCardToHand(Card temp);
		
		// precondition: nothing
		// position: this method will determine if the hand is full
		// return: boolean
		public boolean handIsFull();
		
		
}
